# NSO Workshop - Setup Instructions

**These are setup instructions for workshop instructors**

## NSO base and NED setup

### nso-docker
```sh
mkdir ~/workspace
cd ~/workspace
git clone git@gitlab.com:nso-developer/nso-docker.git
cd nso-docker
​
# Reconfigure upstream
git remote rename origin upstream
git branch -m main
git remote add origin git@gitlab.com:internet2-workshops/nso-cx23/nso-docker.git
git push origin -u
​
# Copy installer
cp ~/nso-files/nso-6.0.linux.x86_64.installer.bin nso-install-files
​
# Build and tag
make build NSO_VERSION=6.0
make tag-release NSO_VERSION=6.0
```
​
### nid-ned-iosxr-cli
```sh
# Create skeleton, push to repository
cd ~/workspace
cp -r nso-docker/skeletons/ned nid-ned-iosxr-cli
cd nid-ned-iosxr-cli
git init .
git branch -m main
git add .
git commit -m 'New NED skeleton'
git remote add origin git@gitlab.com:internet2-workshops/nso-cx23/nid-ned-iosxr-cli.git
git push origin main:main -u
​
# Install NED
(cd packages; tar -xf ~/nso-files/ncs-6.0-cisco-iosxr-7.43.tar.gz)
​
# Cleanup
#   This doesn't work unless NSO is installed, but it's something you'd do before
#   committing the tree and pushing into your repo to avoid committing build artifacts
#   since nso-docker completely rebuilds the NED.
make -C packages/cisco-iosxr-cli-7.43/src clean
​
# Normally we'd add the NED to the source repository and push up to origin, but
# the listed repository is public, so we can't do that
​
# Build & tag NED
make build NSO_VERSION=6.0
make tag-release NSO_VERSION=6.0
```
​
## NSO system setup

**Setup once to create the framework and customize.**

### nid-system
```sh
cd ~/workspace
cp -r nso-docker/skeletons/system nid-system
cd nid-system
git init .
git branch -m main
git add .
git commit -m 'New System skeleton'
​
# Tell the build system to include a NED in the build
echo '${PKG_PATH}nid-ned-iosxr-cli/package:${NSO_VERSION}' > includes/nid-ned-iosxr-cli
​
# Add the following lines to nidvars.mk
export NSO_VERSION ?= 6.0
export NETSIM_IOSXR_IMAGE ?= $(PKG_PATH)nid-ned-iosxr-cli/netsim:$(NSO_VERSION)
export CREATE_MM_TAG = false
​
# Add the following lines to the top of testenvs/quick/Makefile, just under the includes
export DOCKER_NSO_ARGS += --ulimit nofile=1048576
export DOCKER_NED_ARGS += --ulimit nofile=1048576
​
# Add the following lines to testenvs/quick/Makefile, in the 'start:' section just before wait-nso-started
	# Start NETSIM containers
	docker run -td --name $(CNT_PREFIX)-netsim-xr0 --network-alias xr0 $(DOCKER_ARGS) $(DOCKER_NED_ARGS) $(NETSIM_IOSXR_IMAGE)
	docker run -td --name $(CNT_PREFIX)-netsim-xr1 --network-alias xr1 $(DOCKER_ARGS) $(DOCKER_NED_ARGS) $(NETSIM_IOSXR_IMAGE)
	docker run -td --name $(CNT_PREFIX)-netsim-xr2 --network-alias xr2 $(DOCKER_ARGS) $(DOCKER_NED_ARGS) $(NETSIM_IOSXR_IMAGE)
	docker run -td --name $(CNT_PREFIX)-netsim-xr3 --network-alias xr3 $(DOCKER_ARGS) $(DOCKER_NED_ARGS) $(NETSIM_IOSXR_IMAGE)
​
# Add the following lines to testenvs/quick/Makefile, in the 'start:' section, just after wait-nso-started
	# Get lab devices hooked up to NSO
	docker cp config-devices-iosxr.txt $(CNT_PREFIX)-nso:/tmp/config-devices-iosxr.txt
	$(MAKE) runcmdJ CMD="configure\n load merge /tmp/config-devices-iosxr.txt\n commit"
	$(MAKE) runcmdJ CMD="request devices fetch-ssh-host-keys"
	$(MAKE) runcmdJ CMD="request devices sync-from"
	
	# Set up some basic config on the netsims
	docker cp config-interfaces-iosxr.txt $(CNT_PREFIX)-nso:/tmp/config-interfaces-iosxr.txt
	$(MAKE) runcmdJ CMD="configure\n load merge /tmp/config-interfaces-iosxr.txt\n commit"
​
# Create files with appropriate content
testenv/quick/config-devices-iosxr.txt
testenv/quick/config-interfaces-iosxr.txt

# Build
make build
​
# Start testenv
make testenv-start
```

## Rebase-o-rama

```sh
git checkout 00-intro
git rebase main
git push --force-with-lease

git checkout 01-package-created
git rebase 00-intro
git push --force-with-lease

git checkout 02-yang-modified
git rebase 01-package-created
git push --force-with-lease

git checkout 03-template-modified
git rebase 02-yang-modified
git push --force-with-lease

```
