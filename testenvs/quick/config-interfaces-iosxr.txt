ncs:devices {
    ncs:device xr0 {
        ncs:config {
            cisco-ios-xr:interface {
                cisco-ios-xr:GigabitEthernet 0/0;
                cisco-ios-xr:GigabitEthernet 0/1;
                cisco-ios-xr:GigabitEthernet 0/2;
                cisco-ios-xr:GigabitEthernet 0/3;
                cisco-ios-xr:TenGigE 1/0;
                cisco-ios-xr:TenGigE 1/1;
                cisco-ios-xr:TenGigE 1/2;
                cisco-ios-xr:TenGigE 1/3;
                cisco-ios-xr:FortyGigE 2/0;
                cisco-ios-xr:FortyGigE 2/1;
                cisco-ios-xr:FortyGigE 2/2;
                cisco-ios-xr:FortyGigE 2/3;
                cisco-ios-xr:HundredGigE 3/0;
                cisco-ios-xr:HundredGigE 3/1;
                cisco-ios-xr:HundredGigE 3/2;
                cisco-ios-xr:HundredGigE 3/3;
                cisco-ios-xr:FourHundredGigE 4/0;
                cisco-ios-xr:FourHundredGigE 4/1;
                cisco-ios-xr:FourHundredGigE 4/2;
                cisco-ios-xr:FourHundredGigE 4/3;
            }
        }
    }
    ncs:device xr1 {
        ncs:config {
            cisco-ios-xr:interface {
                cisco-ios-xr:GigabitEthernet 0/0;
                cisco-ios-xr:GigabitEthernet 0/1;
                cisco-ios-xr:GigabitEthernet 0/2;
                cisco-ios-xr:GigabitEthernet 0/3;
                cisco-ios-xr:TenGigE 1/0;
                cisco-ios-xr:TenGigE 1/1;
                cisco-ios-xr:TenGigE 1/2;
                cisco-ios-xr:TenGigE 1/3;
                cisco-ios-xr:FortyGigE 2/0;
                cisco-ios-xr:FortyGigE 2/1;
                cisco-ios-xr:FortyGigE 2/2;
                cisco-ios-xr:FortyGigE 2/3;
                cisco-ios-xr:HundredGigE 3/0;
                cisco-ios-xr:HundredGigE 3/1;
                cisco-ios-xr:HundredGigE 3/2;
                cisco-ios-xr:HundredGigE 3/3;
                cisco-ios-xr:FourHundredGigE 4/0;
                cisco-ios-xr:FourHundredGigE 4/1;
                cisco-ios-xr:FourHundredGigE 4/2;
                cisco-ios-xr:FourHundredGigE 4/3;
            }
        }
    }
    ncs:device xr2 {
        ncs:config {
            cisco-ios-xr:interface {
                cisco-ios-xr:GigabitEthernet 0/0;
                cisco-ios-xr:GigabitEthernet 0/1;
                cisco-ios-xr:GigabitEthernet 0/2;
                cisco-ios-xr:GigabitEthernet 0/3;
                cisco-ios-xr:TenGigE 1/0;
                cisco-ios-xr:TenGigE 1/1;
                cisco-ios-xr:TenGigE 1/2;
                cisco-ios-xr:TenGigE 1/3;
                cisco-ios-xr:FortyGigE 2/0;
                cisco-ios-xr:FortyGigE 2/1;
                cisco-ios-xr:FortyGigE 2/2;
                cisco-ios-xr:FortyGigE 2/3;
                cisco-ios-xr:HundredGigE 3/0;
                cisco-ios-xr:HundredGigE 3/1;
                cisco-ios-xr:HundredGigE 3/2;
                cisco-ios-xr:HundredGigE 3/3;
                cisco-ios-xr:FourHundredGigE 4/0;
                cisco-ios-xr:FourHundredGigE 4/1;
                cisco-ios-xr:FourHundredGigE 4/2;
                cisco-ios-xr:FourHundredGigE 4/3;
            }
        }
    }
    ncs:device xr3 {
        ncs:config {
            cisco-ios-xr:interface {
                cisco-ios-xr:GigabitEthernet 0/0;
                cisco-ios-xr:GigabitEthernet 0/1;
                cisco-ios-xr:GigabitEthernet 0/2;
                cisco-ios-xr:GigabitEthernet 0/3;
                cisco-ios-xr:TenGigE 1/0;
                cisco-ios-xr:TenGigE 1/1;
                cisco-ios-xr:TenGigE 1/2;
                cisco-ios-xr:TenGigE 1/3;
                cisco-ios-xr:FortyGigE 2/0;
                cisco-ios-xr:FortyGigE 2/1;
                cisco-ios-xr:FortyGigE 2/2;
                cisco-ios-xr:FortyGigE 2/3;
                cisco-ios-xr:HundredGigE 3/0;
                cisco-ios-xr:HundredGigE 3/1;
                cisco-ios-xr:HundredGigE 3/2;
                cisco-ios-xr:HundredGigE 3/3;
                cisco-ios-xr:FourHundredGigE 4/0;
                cisco-ios-xr:FourHundredGigE 4/1;
                cisco-ios-xr:FourHundredGigE 4/2;
                cisco-ios-xr:FourHundredGigE 4/3;
            }
        }
    }
}
